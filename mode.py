# coding: utf-8
from sqlalchemy import Column, DateTime, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import create_engine



engine = create_engine("mysql+pymysql://root:123456789@1@122.114.69.120:3306/cp?charset=utf8", max_overflow=5, encoding='utf-8')


Base = declarative_base()


class CcLuckyairship(Base):
    __tablename__ = 'cc_luckyairship'

    id = Column(Integer, primary_key=True)
    per = Column(String(32), nullable=False, server_default=text("''"))
    Numbers = Column(String(255), nullable=False, server_default=text("''"))
    openedDate = Column(DateTime, nullable=False)
    addtime=Column(DateTime,nullable=True)


class CcReport(Base):
    __tablename__ = 'cc_report'

    id = Column(Integer, primary_key=True)
    No = Column(Integer, index=True)
    per = Column(String(32))
    content = Column(String(1024))
    idx = Column(Integer)
    status = Column(Integer, server_default=text("0"))
    updated_at = Column(Integer)
    couter = Column(String(512))
    mst_common = Column(String(256))


class CcTimeline(Base):
    __tablename__ = 'cc_timeline'

    id = Column(Integer, primary_key=True)
    per = Column(String(32))
    content = Column(String(512))
    start = Column(String(32))
    created_at = Column(Integer)
    danshuang = Column(Integer)
    daxiao = Column(Integer)
    tzm12 = Column(String(256))
    tzm30 = Column(String(256))
    deleted_at = Column(Integer)


class CcTimelineCopy(Base):
    __tablename__ = 'cc_timeline_copy'

    id = Column(Integer, primary_key=True)
    per = Column(String(32))
    content = Column(String(512))
    created_at = Column(Integer)
    deleted_at = Column(Integer)
    updated_at = Column(Integer)


Base.metadata.create_all(engine)

DBSession = sessionmaker(bind=engine)
